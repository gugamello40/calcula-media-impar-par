<?php
$nomeArquivoCSV = 'data.csv';

$dadosCSV = array();

if (($handle = fopen($nomeArquivoCSV, 'r')) !== FALSE) {
    while (($data = fgetcsv($handle, null, ',')) !== FALSE) {
        $dadosCSV[] = $data;
    }
    fclose($handle);

    foreach ($dadosCSV as $dados) {

        $resultado = separaPontoEVirgula($dados[0]);

        $media = calculaMedia($resultado[0], $resultado[1]);

        $imparPar = verificaImparPar($media);

        echo "A média das colunas numéricas é: $media - $imparPar. \n";

    }
} else {
    echo "Erro ao abrir o arquivo CSV.";
}


function separaPontoEVirgula($linha)
{
    $array = explode(";", $linha);
    return $array;
}

function calculaMedia($a, $b)
{
    return (verificaSeE1($a) + verificaSeE1($b)) / 2;
}

function verificaSeE1($numero)
{
    if (substr($numero, 3, 1) == "1") {
        return 1;
    } else {
        return intval($numero);
    }
}

function verificaImparPar($numero)
{
    if ($numero % 2 == 0) {
        return "Par";
    } else {
        return "Ímpar";
    }
}
